<?php get_header(); ?>
<div id="content">

<div class="postsbody">
<div class="postarea">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php echo get_post_meta($post->ID, "embed", true); ?>
<div class="headpost">
<h1><?php the_title(); ?></h1>
<div class="detail">
<span class="left"><i class="icon-user"></i> <?php the_author(); ?> / <i class="icon-calendar"></i> <?php the_time('F jS, Y'); ?>, <?php the_time('g:i a'); ?> / <i class="icon-folder-open"></i> <?php the_category(' '); ?></span>
<span class="right"><div class="fb-like" data-href="<?php the_permalink(); ?>" data-layout="button" data-action="like" data-show-faces="false" data-share="true"></div></span>
</div>
</div>
<div class="imgpost">
<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('large',array('title' => ''.get_the_title().'' )); ?></a>
</div>
<div class="bodypost">
<?php the_content(); ?>
<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
</div>
<div class="tagpost"><?php the_tags('Tags: ', ', ', '<br />'); ?></div>
</div>
<?php endwhile; endif; ?>

<?php $orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {
$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 5, // Number of related posts that will be displayed.
'caller_get_posts'=>1,
'orderby'=>'rand' // Randomize the posts
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
echo '<div class="related"><span>Related Post</span>';
while( $my_query->have_posts() ) {
$my_query->the_post(); ?>
<div class="postslist">
<div class="left"><h2><a href="<?php the_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2> / <?php the_author(); ?> / <?php the_time('F jS, Y'); ?>, <?php the_time('g:i a'); ?></div>
<div class="right"><?php echo $post->comment_count?><?php echo ($post->comment_count==1?' comment':' comments');?></div>
</div>
<? }
echo '</div>';
} }
$post = $orig_post;
wp_reset_query(); ?>

<div class="disqusmen">
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
<?php echo get_post_meta($post->ID, "embed", true); ?>
<?php comments_template(); ?>
<?php endwhile; endif; ?>
</div>
</div>



<?php include (TEMPLATEPATH . '/sidebar_right.php'); ?>


</div>
</div>


<?php get_footer(); ?>